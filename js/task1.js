
function startTask() {
    
    let user = createNewUser()

    if(user)
    {
        console.log(user)
        console.log('login is:',user.getLogin())
        user.setFirstName('new name')
        user.setLastName('new lastName')
        console.log(user)
    }
    else
    {
        console.error('smth gone wrong!')
    }
  

}
function createNewUser()
{
    let firstName = prompt('Введи Имя'),
        lastName = prompt('Введи Фамилию')

    while(!isNaN(firstName) || !isNaN(lastName))
    {
        firstName = prompt('Введи Имя снова',firstName),
        lastName = prompt('Введи Фамилию снова',lastName)
    }

    const user = {
        firstName,
        lastName,
        getLogin: function()
        {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        },
        setFirstName(newName) //additional task
        {
            console.log('Меняем имя на ',newName)
            if(newName)
            {
                this.setPropValue('firstName',newName) 
            }
           
        },
        setLastName(newLastName)  //additional task
        {
            console.log('Меняем фамилию на ',newLastName)
            if(newLastName)
            {
                this.setPropValue('lastName',newLastName) 
            }
        },
        setPropValue(propName,propValue)  //additional task
        {
            Object.defineProperty(
                this,
                propName,
                {
                    value: propValue,
                },   
            )
        }
    }

    //additional task

    Object.defineProperties(
        user,
        {
            'firstName': {
                writable: false
            },   
            'lastName': {
                writable: false
            },   
        }      
    )

    return user
}